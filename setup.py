#!/usr/bin/env python

import setuptools


setuptools.setup(
    name='Rumba',
    version='0.7',
    url='https://gitlab.com/arcfire/rumba',
    keywords='rina measurement testbed',
    author='Sander Vrijders',
    author_email='sander.vrijders@ugent.be',
    license='LGPL',
    description='Rumba measurement framework for RINA',
    packages=['rumba', 'rumba.testbeds', 'rumba.prototypes', 'rumba.executors'],
    install_requires=[
        'paramiko',
        'docker',
        'repoze.lru; python_version<"3.2"',
        'contextlib2; python_version<"3.0"',
        'enum34; python_version<"3.0"'
    ],
    extras_require={'NumpyAcceleration': ['numpy']},
    scripts=['tools/rumba-access']
)
