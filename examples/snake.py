#!/usr/bin/env python

# An example script using the rumba package

from rumba.model import *
from rumba.utils import ExperimentManager

# import testbed plugins
import rumba.testbeds.emulab as emulab
import rumba.testbeds.jfed as jfed
import rumba.testbeds.local as local
import rumba.testbeds.qemu as qemu

# import prototype plugins
import rumba.prototypes.ouroboros as our
import rumba.prototypes.rlite as rl
import rumba.prototypes.irati as irati

import argparse
import sys


description = "Script to create a snake"

argparser = argparse.ArgumentParser(description = description)
argparser.add_argument('-n', '--nodes', type = int, default = '10',
                       help = "Total number of nodes")

args = argparser.parse_args()

log.set_logging_level('DEBUG')

n01 = NormalDIF("n01")

if (args.nodes < 2):
    print("Snake must be longer than 2 nodes")
    sys.exit(-1)

nodes = []

shim_prev = None
for i in range(0, args.nodes):
    if i is not (args.nodes - 1):
        shim = ShimEthDIF("e" + str(i))
    else:
        shim = None

    if shim_prev == None and shim != None:
        node = Node("node" + str(i), difs = [n01, shim],
                    dif_registrations = {n01 : [shim]})
    elif shim_prev != None and shim != None:
        node = Node("node" + str(i), difs = [n01, shim, shim_prev],
                    dif_registrations = {n01 : [shim, shim_prev]})
    else:
        node = Node("node" + str(i), difs = [n01, shim_prev],
                    dif_registrations = {n01 : [shim_prev]})

    shim_prev = shim
    nodes.append(node)

tb = qemu.Testbed(exp_name = "snake")

exp = rl.Experiment(tb, nodes = nodes)

print(exp)

with ExperimentManager(exp):
    exp.swap_in()
    exp.bootstrap_prototype()
