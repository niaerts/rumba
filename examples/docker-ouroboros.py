#!/usr/bin/env python

# An example script using the rumba package

from rumba.model import *
from rumba.utils import ExperimentManager

# import testbed plugins
import rumba.testbeds.dockertb as docker

# import prototype plugins
import rumba.prototypes.ouroboros as our

import argparse
import sys


description = "Script to create an ouroboros"

argparser = argparse.ArgumentParser(description = description)
argparser.add_argument('-n', '--nodes', type = int, default = '10',
                       help = "Total number of nodes")

args = argparser.parse_args()

log.set_logging_level('DEBUG')

n01 = NormalDIF("n01")

if (args.nodes < 3):
    print("The ouroboros must be longer than 2 nodes")
    sys.exit(-1)

nodes = []

shim_prev = None
for i in range(0, args.nodes):
    shim = ShimEthDIF("e" + str(i))

    if shim_prev == None and shim != None:
        node = Node("node" + str(i), difs = [n01, shim],
                    dif_registrations = {n01 : [shim]})
    elif shim_prev != None and shim != None:
        node = Node("node" + str(i), difs = [n01, shim, shim_prev],
                    dif_registrations = {n01 : [shim, shim_prev]})
    else:
        node = Node("node" + str(i), difs = [n01, shim_prev],
                    dif_registrations = {n01 : [shim_prev]})

    shim_prev = shim
    nodes.append(node)

nodes[0].add_dif(shim_prev)
nodes[0].add_dif_registration(n01, shim_prev)

tb = docker.Testbed(exp_name = "ouroboros",
                    base_image = "ouroborosrumba/prototype")

exp = our.Experiment(tb, nodes = nodes)

print(exp)

with ExperimentManager(exp):
    exp.swap_in()
    exp.bootstrap_prototype()
