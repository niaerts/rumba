#!/usr/bin/env python

# An example script leveraging the storyboard scripting functionality

from rumba.storyboard import *
from rumba.model import *
import rumba.log as log
import rumba.testbeds.qemu as qemu
import rumba.prototypes.rlite as rl
from rumba.utils import ExperimentManager
import rumba.utils as utils

log.set_logging_level(log.DEBUG)


n1 = NormalDIF("n1")

n1.add_policy("rmt.pff", "lfa")
n1.add_policy("security-manager", "passwd")

e1 = ShimEthDIF("e1")

node_a = Node("node_a",
              difs=[n1, e1],
              dif_registrations={n1: [e1]})

node_b = Node("node_b",
              difs=[e1, n1],
              dif_registrations={n1: [e1]})

tb = qemu.Testbed(exp_name="script_test",
                  username="root",
                  password="root")

exp = rl.Experiment(tb, nodes=[node_a, node_b])


client_a = Client(
    "rinaperf",
    options="-t perf -s 1000 -D <duration>",
    shutdown="",
    c_id='rinaperf_a'
)

client_b = Client(
    "rinaperf",
    options="-t perf -s 1000 -D <duration> -z rinaperfb",
    shutdown="",
    c_id='rinaperf_b'
)

client_c = Client(
    "rinaperf",
    options="-t perf -s 1000 -D <duration> -z rinaperfc",
    shutdown="",
    c_id='rinaperf_c'
)

server_a = Server(
    "rinaperf",
    options="-l",
    arrival_rate=1,
    mean_duration=5,
    clients=[client_a],
    s_id='server_a'
)

server_b = Server(
    "rinaperf",
    options="-l -z rinaperfb",
    arrival_rate=0.5,
    mean_duration=10,
    clients=[client_b],
    s_id='server_b'
)

server_c = Server(
    "rinaperf",
    options="-l -z rinaperfc",
    arrival_rate=1.6,
    mean_duration=3,
    clients=[client_c],
    s_id='server_c'
)


if __name__ == '__main__':
    story = StoryBoard(30)
    story.set_experiment(exp)
    story.add_server_on_node(server_a, node_a)
    story.add_server_on_node(server_b, node_a)
    story.add_server_on_node(server_c, node_a)
    client_a.add_node(node_b)
    client_b.add_node(node_b)
    client_c.add_node(node_b)
    story.parse_script_file('example-script.rsb')
    log.flush_log()
    with ExperimentManager(exp):
        exp.swap_in()
        exp.bootstrap_prototype()
        story.start()
