#!/usr/bin/env python

# An example script using the rumba package

from rumba.model import *
from rumba.utils import ExperimentManager
from rumba.storyboard import *

# import testbed plugins
import rumba.testbeds.emulab as emulab
import rumba.testbeds.jfed as jfed
import rumba.testbeds.local as local
import rumba.testbeds.qemu as qemu

# import prototype plugins
import rumba.prototypes.ouroboros as our
import rumba.prototypes.rlite as rl
import rumba.prototypes.irati as irati

import rumba.log as log

log.set_logging_level('DEBUG')


n1 = NormalDIF("n1")

n1.add_policy("rmt.pff", "lfa")
n1.add_policy("security-manager", "passwd")

e1 = ShimEthDIF("e1")

a = Node("a",
         difs=[n1, e1],
         dif_registrations={n1: [e1]})

b = Node("b",
         difs=[e1, n1],
         dif_registrations={n1: [e1]})

tb = jfed.Testbed(exp_name="example1",
                  username="user1",
                  cert_file="/home/user1/cert.pem")

exp = rl.Experiment(tb, nodes=[a, b])

print(exp)

# General setup (can be reused in other scripts as-is)
storyboard = StoryBoard(duration=30)

# Clients can be applications that just keep running, and will be
# stopped by a SIGINT...
client1 = Client("rinaperf",
                 options="-t perf -s 1000 -c 0")

# ... or a custom shutdown method can be provided.
client2 = Client("rinaperf",
                 options="-t perf -s 1000 -D <duration>",
                 shutdown="")

server = Server("rinaperf", options="-l", arrival_rate=0.5,
                mean_duration=5, clients=[client1, client2])


# Experiment-specific configuration:
# (This can be done anytime before storyboard.start())

storyboard.set_experiment(exp)
storyboard.add_server_on_node(server, a)
client1.add_node(b)
client2.add_node(b)


print(exp)

with ExperimentManager(exp):
    exp.swap_in()
    exp.install_prototype()
    exp.bootstrap_prototype()
    storyboard.start()

    # Fetch the client/server logs from the nodes
    # and put them in the cwd.
    storyboard.fetch_logs()
